# Controlled vocabularies for TERN-Plot
## Application
Values from **controlled vocabularies** (CV) or **look-up tables** (LUT) may appear in several places, including
- as the results of observations (e.g. taxon value, floristics-structure, geology)
- as the observed-property of observations
- to classify entities within datasets (e.g. site-stratum level, observation-collections, bioregions)

## Formalization
It is desirable that the definition of each member of each CV or LUT be available. To achieve this,
- each CV should be encoded in a suitable machine-readable form
- each term or code should have its own URI
- vocabularies should be hosted so that de-referencing each URI resolves to the definition (i.e. published as 'Linked Data')
- alternative formats should be available (HTML for humans; JSON, TTL and RDF/XML for machines)

[SKOS](https://www.w3.org/TR/skos-primer/) is the default standard for encoding a multi-lingual thesaurus. It supports the recording of
- [alternative terms](https://www.w3.org/TR/skos-primer/#seclabel) for the same concept
- [hierarchical relationships](https://www.w3.org/TR/skos-primer/#sechierarchy) within a vocabulary (broader/narrower terms)
- [mapping relationships](https://www.w3.org/TR/skos-primer/#secmapping) between concepts in separate vocabularies

Other more specific schemes are available for certain scopes (e.g. units of measure using QUDT)

## Harmonization
It is desirable to use shared (standard) codes and definitions as much as possible.

The scope of some are defined external to the TERN application, and external standards may be used - e.g. units of measure, names of jurisdictions, bioregions, geology & soils classifications - (though they may not yet be published in a suitable form).

Vocabularies for which externally-hosted sources should be used
- units of measure (QUDT)
- chemical substances (IUPAC)
- localities, geographical areas, bioregions (from Loc-I)
- lithology types, stratigraphic units (from GA)
- soil classifiers (ASC, ASLS, SCMA)
- landscape classifiers (ASLS)

Within the ecosciences, harmonization of vocabularies between existing data-suppliers may involve merging and splitting of concepts that are broader or narrower than each other, but may involve more complex relationships where concepts relate in non-hierarchical ways. Hence, conversion of data from its original form will almost always be lossy.

Vocabularies that are candidates for full harmonization include
- observable properties
- procedures, protocols, methodologies (various)
- map-scales

Some vocabularies reflect details of the scientific method or interpretation and potentially cannot be harmonized - e.g. the methodology for assessing vegetation strata is based on a different structural classification in different jurisdictions.   

## Sources
- controlled vocabularies that should be [shared across TERN](.)
    - [observable properties](cv/observable-property.ttl)
    - [observation groups](cv/observation-group.ttl)
    - [bioregions](cv/bioregion.ttl)
- controlled vocabularies [used in data supplied by CORVEG](cv/corveg)

## Vocabulary publication
As a proof-of-concept for (a) the use of SKOS (b) distribution as linked-data (i.e. ), some TERN-plot controlled vocabularies are hosted in the [CSIRO Linked Data Registry](http://registry.it.csiro.au/sandbox/tern/plot). 

## Development
Please refrain from changing the identifier names once it has been used in ETL code. If a change is done to an identifier, make sure it is also changed in ETL code where it references the identifier.