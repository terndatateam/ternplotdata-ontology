# TERN Plot Data

The latest core Plot Ontology is now hosted on GitHub at https://github.com/ternaustralia/ontology_tern-plot with online documentation hosted on GitHub Pages. 

A home for the ontology artifacts related to TERN Plot Data.

## AEKOS Ontology
The [AEKOS Ontology](schema/aekos-owl.ttl) serves as the starting point. This was automatically generated from the AEKOS DSL.

## TERN-plot ontology and supporting resources
- [ontology description](schema/)
- [rdf representation](schema/tern-plot.ttl)
- [controlled vocabularies](cv/)

## TERN-plot applications
### CORVEG
CORVEG alignment to TERN-plot

- [description](corveg/)
- [example](examples/corveg-0.ttl)

### AusPlots
AusPlots alignment to TERN-plot

- [description](ausplots/)

### SA-VEG
SA-VEG alignment to TERN-Plot

- [description](sa-veg/)

### OEH Atlas
OEH Atlas alignment to TERN-Plot

- [description](oeh-atlas/)

## How to read an RDF/OWL file

A free industry standard tool for working with ontologies is Protege (https://protege.stanford.edu/). It runs in Java so it will work on all platforms. You can download the desktop version (they also offer a cloud version) of Protege from https://protege.stanford.edu/products.php#desktop-protege.

Most of the information that is available in the OWL file is displayed by Protege. The only exception is that you can't see the long description for the properties. To view those, just open the OWL file in a text editor and find the entry. Currently the long description for the classes isn't written to the OWL file, this is an enhancement that needs to happen to the tool that generates this file.
