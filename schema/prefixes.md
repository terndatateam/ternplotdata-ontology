## Dependencies and namespaces
TERN-plot re-uses elements from a number of well known pre-existing ontologies.
These are listed below along with the new namespaces defined for TERN-plot.

| prefix | source | explanation | governance |
|--|--|--|--|
| `dc:` | [Dublin Core Elements](http://www.dublincore.org/documents/dces/) | classic general purpose metadata | DCMI |
| `dct:` or `dcterms:` | [Dublin Core Terms](http://www.dublincore.org/documents/dcmi-terms/) | full DC vocabulary with domains and ranges | DCMI |
| `foaf:` | http://xmlns.com/foaf/0.1/ | Friend-of-a-friend - people | FOAF |
| `geo:` or `w3cgeo:` | [Basic Geo (WGS84 lat/long) vocabulary](https://www.w3.org/2003/01/geo/) | point geometry | W3C |
| `geosparql:` | [GeoSPARQL](http://www.opengeospatial.org/standards/geosparql) | geometry classes and spatial topology relations. | OGC |
| `gml:` | http://www.opengis.net/ont/gml# | Geography Markup Language - geometry | OGC |
| `locn:` | [Core location vocabulary](https://www.w3.org/ns/locn) | places and addresses | INSPIRE / W3C |
| `prov:` | [Provenance ontology](https://www.w3.org/TR/prov-o/) | Entities, Activities and Agents - process-flow model | W3C |
| `sf:` | http://www.opengis.net/ont/sf# | Simple Features - spatial relations | OGC |
| `skos:` | http://www.w3.org/2004/02/skos/core# | Simple Knowledge Organization System - thesaurus | W3C |
| `ssn:` | [Semantic Sensor Network Ontology](https://www.w3.org/TR/vocab-ssn/) | OWL implementation of O&M, including Sampling, plus Actuations | W3C |
| `ssn-ext:` | [SSN Extensions](https://w3c.github.io/sdw/proposals/ssn-extensions/) | Observation collections; Ultimate feature-of-interest | W3C SDWIG |
| `sosa:` | [Sensors, Observations, Sampling and Actuations](https://www.w3.org/TR/vocab-ssn/) | Core of SSN | W3C |
| `time:` | [OWL-Time](https://www.w3.org/TR/owl-time/) | Temporal entities, ordering and other interval relations | W3C |
| `unit:` | http://qudt.org/vocab/unit/ | units of measure |
| `xsd:` | http://www.w3.org/2001/XMLSchema# | XML Schema datatypes (W3C) |
|  |  |  |  |
| `bioregion:` | http://www.tern.org.au/cv/bioregion/> | Bioregions | TERN |
| `op:` | http://www.tern.org.au/cv/op/ | Observable properties | TERN |
| `cv-corveg:` | http://www.tern.org.au/cv/corveg/ | CORVEG classifiers | TERN/CORVEG |
| `corveg:` | http://www.tern.org.au/ns/corveg/ | Data-types used for CORVEG tagging | TERN/CORVEG |
| `data:` | http://www.tern.org.au/ns/data/ | TERN-plot data-types | TERN |
| `linn:` | http://gbif.example.org/id/linnean/ | Linnean ID | notional |
| `ogroup:` | http://www.tern.org.au/cv/ogroup/ | TERN site characterization themes | TERN |
| `plot:` | [TERN-plot](tern-plot.ttl) | Packaging of all these modules and dependencies, plus extensions for sites and site-visits | TERN |
| `plot-x:` | [TERN-plot extensions](tern-plot.ttl) | Experimental extensions to the basic TERN-plot ontology | TERN |
