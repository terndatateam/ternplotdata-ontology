# RDF details and principles
## Notation
Examples are shown as **RDF** serialized using the [Turtle syntax](https://www.w3.org/TR/turtle/).

## Compact URIs, prefixes
'Colon-ized names' like `sosa:Observation` and `bioregion:qld-CHC` are compact abbreviations for URIs like `http://www.w3.org/ns/sosa/Observation` and `http://www.tern.org.au/cv/bioregion/qld-CHC`. The full URI is built by prepending the URI denoted by the prefix to the name - see [list of prefixes](prefixes.md).  

## Completeness, open world assumption
Following standard RDF principles, the information shown in the examples is incomplete: only the key information needed to illustrate the point is included. Note in particular that **all properties** of an entity can be **repeated an unlimited number of times**, and other properties of interest might be included in real data.

## O&M in RDF
The RDF implementation of O&M is taken from the [W3C Semantic Sensor Network Ontology](https://www.w3.org/TR/vocab-ssn/), supplemented by some elements from the proposed [W3C Extensions to the Semantic Sensor Network Ontology](https://www.w3.org/TR/vocab-ssn-ext/)
