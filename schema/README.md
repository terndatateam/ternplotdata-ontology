# TERN-Plot Ontology

[TERN-plot](../schema/tern-plot.ttl) is an ontology for site-based ecology and biodiversity observations.
It is currently (2018-11) based on the [W3C SSN ontology](https://www.w3.org/TR/vocab-ssn/), with extensions for sites.

The details of the TERN-plot Ontology is factored into several files which are _imported_ into [TERN-plot](../schema/tern-plot.ttl):

- [plot.ttl](../schema/plot.ttl) is the classes and properties composing the primary TERN-plot model
- [data.ttl](../schema/data.ttl) is a set of classes corresponding to 'data-types' used as the results of observations in TERN-plot instances
- [corveg.ttl](../schema/corveg.ttl) has some CORVEG-specific elements to facilitate tracing back to the CORVEG sources

## Dependencies and namespaces
TERN-plot re-uses elements from a number of established existing ontologies.
The core classes and concepts are from the W3C [SSN/SOSA](https://www.w3.org/TR/vocab-ssn/) and [SSN-ext](https://www.w3.org/TR/vocab-ssn-ext/#ssn-ext:ObservationCollection) ontologies. Other dependencies are listed in the key to [namespace prefixes](./prefixes.md).

## Observation and observation collection
The core structures are based around the O&M classes `Observation` and `ObservationCollection`.

![sosa:Observation essentials](../images/observation-attributes.png)

A `sosa:Observation` uses a specified `sosa:Procedure` to obtain the value of an `sosa:ObservableProperty`, at the `sosa:phenomenonTime` indicated, relating to a specified `sosa:FeatureOfInterest`. The `sosa:Result` is the outcome.

A set of observations relating to the same feature-of-interest may be grouped in a `ssn-ext:ObservationCollection`.
In the context of TERN-plot observations are also typically grouped thematically - e.g. stratum, taxon, geology, soil, structure, situation, disturbance, etc - providing details relating to the same theme.

These templates show the pattern.
### Single observation
```
ex:O-9876
  rdf:type sosa:Observation ;
  sosa:hasFeatureOfInterest {URL of site, strata, taxa, etc} ;
  sosa:phenomenonTime {time the result applies} ;
  sosa:hasResult {the actual value} ;
  sosa:observedProperty op:{observable property} ;
  sosa:usedProcedure {URL of procedure description} ;  # optional
  sosa:madeBySensor {URL of sensor description} ;  # optional
  {...}
.
```

### Observations grouped in a theme
With links to the individual Observations with external identifiers:
```
ex:OC-1234
  rdf:type ssn-ext:ObservationCollection ;
  dct:type ogroup:{observation theme} ;
  sosa:hasFeatureOfInterest {URL of site, strata, taxa, etc} ;
  sosa:phenomenonTime {time the result applies} ;
  ssn-ext:hasMember ex:O-654 ;
  ssn-ext:hasMember ex:O-321 ;
    {...}
.
ex:O-654
    rdf:type sosa:Observation ;
    sosa:hasResult {the actual value} ;
    sosa:observedProperty op:{observable property} ;
    sosa:usedProcedure {URL of procedure description} ;  # optional
    sosa:madeBySensor {URL of sensor description} ;  # optional
.
ex:O-321
    rdf:type sosa:Observation ;
    sosa:hasResult {another value} ;
    sosa:observedProperty op:{another observable property} ;
    sosa:usedProcedure {URL of another procedure description} ;  # optional
    sosa:madeBySensor {URL of another sensor description} ;  # optional
.
```

## Customization of the generic observation model
The 'generic' information model for observations is customized for a specific application through the choice of values used for the `sosa:ObservableProperty` and `sosa:Procedure`, and the types used for the `sosa:FeatureOfInterest`.
Use of a generic model (rather than separate classes for different observation types) has the following advantages:
- x-domain compatibility
- less ontological commitment in a context where the axiomatization is not (yet) well developed
- ability to maintain and govern disciplinary semantics (through the controlled-vocabularies) on a more flexible timescale than if it was part of the base ontology

## TERN-plot features of interest (FOI)
The [TERN-plot ontology](../schema/tern-plot.ttl) contains definitions of a _small_ set of entity types, individuals of which then serve as the _feature-of-interest_ of individual observations.
![Features of interest](../images/FOI.PNG)

These classes represent way that a site is conceptualized for observations of the vegetation in an ecological survey, i.e. it is the 'domain model' for vegetation observations.

The starting point is `sosa:Sample`, which is a feature-of-interest that is intended to be representative of a larger feature.
Most of the TERN-plot types are modeled as sub-classes of `sosa:Sample`, because an instance of any of these is primarily of interest because it represents an ecosystem, a community, or an assemblage.

- [plot:Site](Site.md) - which samples an environmental-system or -zone
    - [locn:location](Site.md) - link to the details of a site location
    - [plot:siteDescription](Site.md) - link to primary (invariant) site properties
- `plot:SiteStratumTaxon` - single taxon assemblage in a single stratum at a site
- `plot:SiteStratum` - summary of single stratum at a site
- `plot:SiteTaxon` - summary of a single taxon at a site
- `plot:OrganismOccurrence` - single organism
- `plot:SiteVisit` - see Events below

Most properties of an instance of any FOI are the results of observations relating to specified [observable properties](ObservableProperty.md) associated with the entity.

## TERN-plot controlled vocabularies
Application-specific classifiers are managed as [controlled vocabularies](../cv/).

The [Observable Property vocabulary]() lists all the _variables/determinands/analytes_ for which data is available. It is expected that a common set of Observable Properties can be established for TERN.

Other vocabularies classify additional elements of domain practice, such as
- _procedures and protocols_, e.g.
    - [cover-methods](cv/corveg/cover-method.ttl)
    - [geology-sources](cv/corveg/geology-source.ttl)
    - [soil-sources](cv/corveg/soil-source.ttl)
    - [taxa-identifications](cv/corveg/taxa-identification.ttl)
- _instruments or sensors_

These might be standardized at a finer grain (e.g. CORVEG vs SA-VEG vs OEH-Atlas vs AusPlots), though there will usually be significant overlap which might be factored into some common vocabularies.

Finally, the _value-space_ for the results of many of the observations can be managed as a controlled vocabulary (e.g. taxon list, soil-type, landscape-type) or must be qualified by reference to a value from a controlled vocabulary (e.g. units-of-measure). In many cases these vocabularies are specified by an external organization. If these organizations provide persistent web identifiers for the items in the vocabularies, they should be used directly. If not, then alternative hosting arrangements should be made.

Some of the provisional vocabularies are already available from [CSIRO's Linked Data Registry](http://registry.it.csiro.au/sandbox/tern/plot) so as to illustrate the principle of distributing them as "Linked Data", including:
- [observation group classification](http://registry.it.csiro.au/sandbox/tern/plot/ogroup) - which provide values for the `dct:type` of observation collections
- [TERN bioregions](http://registry.it.csiro.au/sandbox/tern/plot/bioregion)
- [IBRA7](http://registry.it.csiro.au/sandbox/tern/plot/IBRA7)

## Observation results
The O&M observation information model (or its implementation in SSN/SOSA) does not define or prescribe any specific result-types. The [TERN-plot ontology](../schema/tern-plot.ttl) therefore uses the following sub-classes of `sosa:Result`

- `data:Boolean`  
- `data:Text`
- `skos:Concept`
- `data:Quantitative-measure`
- `data:Percent`
- `data:Quantitative-range`
- `data:Percent`
- `w3cgeo:Point`

Details of these are described in [result-types](result-types.md).

## Events, visits, activities
Several of the key classes in the [TERN-plot ontology](../schema/tern-plot.ttl) are kinds of _activity_.
These are modeled as sub-classes of `prov:Activity`, with dependencies expressed as sub-activities.  
![Activities](../images/Activities.PNG)

A `plot:SiteVisit` is usually made for the purposes of collecting samples or making observations. Hence, the sub-activities of a site-visit will be acts of sampling (i.e. `sosa:Sampling`) or of observation (i.e. `sosa:Observation`) which may be grouped in collections (i.e. an `ssn-ext:ObservationCollection`). The site-visit itself can be given an additional `rdf:type ssn-ext:ObservationCollection` so that all the member observations can inherit a common feature of interest (i.e. the Site) and phenomenon-time.  


## Alignment of TERN-plot to supplier schemas

- [CORVEG](../corveg/)
- [AusPlots](../ausplots)
- [SA-VEG](../sa-veg/)
- [OEH Atlas](../oeh-atlas/)

## Alignment to OBO
A future iteration of `TERN-plot` may transition to [OBO Foundry ontologies](http://www.obofoundry.org/), in particular [OBI - Ontology for Biomedical Investigations](http://www.obofoundry.org/ontology/obi.html), [BCO - Biological Collections Ontology](http://www.obofoundry.org/ontology/bco.html) and [ENVO - Environment Ontology](http://www.obofoundry.org/ontology/envo.html).
The OBO framework is semantically rigorous and is broadly respected and used in the ecological community.
However, the relevant OBO ontologies (OBI, BCO, ENVO) are currently incomplete with respect to the requirements relating to sites and site visits.
We have an active engagement with OBO to resolve this - see https://github.com/BiodiversityOntologies/bco/issues/91 https://github.com/BiodiversityOntologies/bco/issues/86 https://github.com/BiodiversityOntologies/bco/issues/92 https://github.com/BiodiversityOntologies/bco/issues/93 - but it is now clear that these changes/additions to OBO ontologies will not be complete on the time-frame necessary for refreshing AEKOS.

Note that alignment of the widely-used [Darwin Core](http://rs.tdwg.org/dwc/terms/) vocabulary to OBO is already underway.
