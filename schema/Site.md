# plot:Site
![Features of interest](../images/FOI.PNG)

A `plot:Site` is designed to sample an ecosystem at some scale, the identity of which is indicated using the standard `sosa:isSampleOf` property.
A site is established by a site-visit (`sosa:isResultOf`) which is in turn part of a survey (`plot:wasSubActivityOf`).
A site may be related to another site (`dct:relation`), be part-of another site (i.e. a sub-site - `dct:isPartOf`) or may have sub-sites (`dct:hasPart`).

## Location
The site location is described using an instance of `plot:Location`. This includes the source map information, and the location coordinates (using the `locn:geometry` property from the standard location vocabulary).

See [Location example for CORVEG data](../corveg/corveg-location.md).

## Site description
The basic description of the site is given using a `ssn-ext:ObservationCollection` for which `dct:type = ogroup:Site-description`.  

See [Site description example for CORVEG data](../corveg/corveg-site.md).

## Other site properties and descriptors
All other properties of a site are the results of observations having this site as its feature-of-interest, typically grouped collections relating to geology, soil, disturbance, etc.

See [examples of various observation collections for CORVEG data](../corveg/).
