# Data types for observation results

The result of an `sosa:Observation` must be encapsulated as an individual RDF Resource (i.e. an object, not a Literal) from the class `sosa:Result`.  

If the result of an observation is an item from an existing set, such as a controlled-vocabulary, then the URI for the item can be used directly, e.g. 
```
ex:O-997
    rdf:type sosa:Observation ;
    sosa:hasResult <http://anzsoil.org/def/au/asls/soil-profile/field-texture-CL> .
    sosa:observedProperty op:soil-texture ;
  .
```

If the result of an observation is a quantity or truth value, then its value is encoded using a local individual (blank-node) of a type that is a sub-class of `sosa:Result`.
For example:   
```
ex:O-998
    rdf:type sosa:Observation ;
    sosa:hasResult [
        rdf:type data:Percent ;
        rdf:value "67"^^xsd:decimal ;
      ] ;
    sosa:observedProperty op:cover-bare ;
  .
```
A small set of types are provided in TERN-plot for convenience.
The actual value is usually encoded as its `rdf:value`, except where the result is a **value range** which may have up to two values in `data:low` and `data:high`.

These data-types are not part of TERN-plot semantics so they are defined in a separate namespace. If another set of data-types are available they may be used in preference.

## Quantitative values
### data:Count
Use when the observation result reports a count.
```
<> sosa:hasResult [
  rdf:type data:Count ;
  rdf:value "17"^^xsd:integer ;
] .
```
> In this and the other examples, ``<>`` is shorthand for a local URI denoting the observation.

### data:Quantitative-measure
Use when the observation result is a scaled number.
The scale or unit-of-measure is given by the `data:standard` (here `unit:PER-HA` is short for http://qudt.org/vocab/unit/PER_HA)
```
<> sosa:hasResult [
  rdf:type data:Quantitative-measure ;
  data:standard unit:PER-HA ;
  rdf:value "330"^^xsd:decimal ;
] .
```
If a local error or uncertainty is available, then use the optional `data:uncertainty` property, for example if the error is 5 ha-1 
```
<> sosa:hasResult [
  rdf:type data:Quantitative-measure ;
  data:standard unit:PER-HA ;
  data:uncertainty "5"^^xsd:decimal ;
  rdf:value "330"^^xsd:decimal ;
] .
```

### data:Percent
Use when the observation result is a percentage.
The scale is fixed to http://qudt.org/vocab/unit/PERCENT.
```
<> sosa:hasResult [
  rdf:type data:Percent ;
  rdf:value "18"^^xsd:decimal ;
] .
```

### data:Quantitative-range
Use when the observation result is a scaled number range.
The scale or unit-of-measure is given by the `data:standard` (here `unit:PER-HA` is short for http://qudt.org/vocab/unit/PER_HA)
```
<> sosa:hasResult [
  rdf:type data:Quantitative-range ;
  data:high "9"^^xsd:decimal ;
  data:low "6"^^xsd:decimal ;
  data:standard unit:M ;
] .
```
Note that all values are optional so this type can be used to record an open-ended range, e.g.
```
<> sosa:hasResult [
  rdf:type data:Quantitative-range ;
  data:low "4.1"^^xsd:decimal ;
  data:standard unit:M ;
] .
```

### data:Percent-range
Use when the observation result is a percentage range.
The scale is fixed to http://qudt.org/vocab/unit/PERCENT .
```
<> sosa:hasResult [
  rdf:type data:Percent-range ;
  data:high "42"^^xsd:decimal ;
  data:low "16"^^xsd:decimal ;
] .
```

## Truth, presence/absence
### data:Boolean
Use when the observation result is true/false, such as presence/absence.
```
<> sosa:hasResult [
  rdf:type data:Boolean ;
  rdf:value "false"^^xsd:boolean ;
] .
```

## Concepts
### data:Text
Use when the observation result is free text.
```
<> sosa:hasResult [
  rdf:type data:Text ;
  rdf:value "Qpa"^^xsd:string ;
] .
```
> Always use a value from a controlled vocabulary in preference to free text if possible.

### skos:Concept
Use when the observation result is a formally defined Concept, including taxon identifications.

#### Locally defined concept
Where the desired concept is not currently available as linked-data, it can be embedded as a blank-node of `rdf:type skos:Concept`
```
<> sosa:hasResult [
  rdf:type skos:Concept ;
  skos:definition "... full textual definition included here ..."^^xsd:string ;
  skos:inScheme <http://example.geoscience.au/stratlex> ;
  skos:notation "Qpa" ;
  skos:prefLabel "Quaternary alluvium"^^xsd:string ;
] .
```

#### Concept from a controlled vocabulary
Where there is an existing linked-data definition, its URI allows it to be referred to directly:  
```
<> sosa:hasResult <http://anzsoil.org/def/au/asls/soil-profile/field-texture-CL> .
```
