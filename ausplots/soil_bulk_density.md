# Soil bulk density

![soil_bulk_density schema](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/diagrams/soil_bulk_density.1degree.png)


## Provisional Mapping from soil_bulk_density to TERN-plot
A row from an AusPlots `soil_bulk_density` table is mapped to an individual `ssn-ext:ObservationCollection`

| soil_bulk_density | TERN-plot | comments |
| --- | --- | --- |
| id | localname (last element of the URI) | the AusPlots ID may not need to be preserved |
| site_location_visit_id | `plot:wasSubActivityOf` | link to associated `plot:SiteVisit` |
| sample_id | `sosa:hasFeatureOfInterest` | pointer to the actual sample |
| paper_bag_weight | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:weight-of-bag` <br/> and encode value using `data:Quantitative-measure` |
| oven_dried_weight_in_bag | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:weight-oven-dried-in-bag` <br/> and encode value using `data:Quantitative-measure` |
| ring_weight | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:weight-of-ring` <br/> and encode value using `data:Quantitative-measure` |
| ring_volume | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:volume-of-ring` <br/> and encode value using `data:Quantitative-measure` |
| gravel_volume | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:volume-of-gravel` <br/> and encode value using `data:Quantitative-measure` |
| fine_earth_weight_in_bag | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:weight-fine-earth-in-bag` <br/> and encode value using `data:Quantitative-measure` |
| fine_earth_weight | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:weight-fine-earth` <br/> and encode value using `data:Quantitative-measure` |
| fine_earth_volume	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:volume-fine-earth` <br/> and encode value using `data:Quantitative-measure` |
| fine_earth_bulk_density	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:density-fine-earth` <br/> and encode value using `data:Quantitative-measure` |
| gravel_bulk_density	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:density-gravel` <br/> and encode value using `data:Quantitative-measure` |

Strictly speaking each of the member observations is related to a sub-sample of the bulk soil sample, so the feature-of-interest of each member observation could be characterized separately. In practice we can manage this though a complex set of observable-properties.
