# Alignment of AusPlots to TERN-plot
This alignment relates the [AusPlots logical model](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/) to the [O&M conceptual model](../schema).

<!---
![AusPlots database schema](./relationships.real.large.png)
-->

AusPlots records a large set of observations of different properties of a [site_location](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/site_location.html), associated with an explicit [site_location_visit](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/site_location_visit.html). Each of these classes has attributes that can be understood as mixture of
1. 'standard' properties, like **established_date**, **bioregion_name**, **visit_start_date**, **visit_notes**, and
- properties whose values can be understood as the results of observations, e.g. **landform_pattern**, **site_slope**, **outcrop_lithology**, **erosion_type**, **disturbance**, **a_s_c**
- groups of properties that appear to imply a relationship with an entity of an type, e.g. **pit_marker**.

A visit is also associated with
1. a set of discrete observations encoded using distinct classes - e.g. [visit_surface_coarse_frags_lithology](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/visit_surface_coarse_frags_lithology.html), [visit_surface_coarse_frags_size](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/visit_surface_coarse_frags_size.html), [visit_surface_coarse_frags_abundance](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/visit_surface_coarse_frags_abundance.html)
2. a set of standard observation collections, including [structural_summary](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/structural_summary.html), [soil_characterisation](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/soil_characterisation.html), [soil_bulk_density](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/tables/soil_bulk_density.html),


These are mapped to TERN-plot using the as follows:

- [site_location mapping](./site_location.md)
- [site_location_point mapping](./site_location_point.md)
- [site_location_visit mapping](./site_location_visit.md)
- [visit_surface_coarse_frags_lithology mapping](./visit_surface_coarse_frags_lithology.md)
- [visit_surface_coarse_frags_size mapping](./visit_surface_coarse_frags_size.md)
- [visit_surface_coarse_frags_abundance mapping](./visit_surface_coarse_frags_abundance.md)
- [visit_surface_coarse_frags_type mapping](./visit_surface_coarse_frags_type.md)
- [visit_surface_soil_conditions mapping](./visit_surface_soil_conditions.md)
- [structural_summary mapping](./structural_summary.md)
- [soil_characterisation mapping](./soil_characterisation.md)
- [soil_bulk_density mapping](./soil_bulk_density.md)

TBC
