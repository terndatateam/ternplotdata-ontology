# Site location

![site_location schema](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/diagrams/site_location.1degree.png)


## Provisional Mapping from site_location to TERN-plot
A row from an AusPlots `site_location` table is usually mapped to an individual `plot:Site` and an associated `plot:siteDescription/ssn-ext:ObservationCollection` and `locn:location/plot:Location` with the columns mapped to TERN-plot properties as follows:

| site_location | TERN-plot | comments |
| --- | --- | --- |
| site_location_id | localname (last element of the URI) | the AusPlots ID may not need to be preserved |
| site_location_name | `rdfs:label` |  |
| established_date | `prov:generatedAtTime` |  |
| description | `dct:description` |  |
| bioregion_name | `ssn-ext:hasUltimateFeatureOfInterest` |  |
| property | _not encoded directly in TERN-plot_ | Is this part of the site address? Additional properties of `plot:Location` ?  |
| paddock | _not encoded directly in TERN-plot_ | Is this part of the site address? Additional properties of `plot:Location` ? |
| plot_is_permanently_marked | _not encoded directly in TERN-plot_ |  |
| plot_is_aligned_to_grid | _not encoded directly in TERN-plot_ |  |
| plot_is_100m_by_100m | _not encoded directly in TERN-plot_ |  |
| landform_pattern | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:landform-pattern` <br/> and use a value from the collection `cv-corveg:landform-patterns` |
| landform_element | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:landform-element` <br/> and use a value from the collection `cv-corveg:landform-elements` |
| site_slope | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:slope-angle` <br/> and encode value using `data:Quantitative-measure` |
| site_aspect | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:slope-aspect` <br/> and encode value using `data:Quantitative-measure` |
| plot_dimensions | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:area` <br/>and encode value using `data:Quantitative-measure` <br/>_but is this more than area?_ |
| comments | `rdfs:comment` |  |
| app_lat | `locn:location/locn:geometry/geo:lat` |  |
| app_long | `locn:location/locn:geometry/geo:long` |  |
| outcrop_lithology | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:geology-lithology` <br/> and use a value from the collection `cv-corveg:geology-types` |
| other_outcrop_lithology | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:geology-lithology` <br/> and use a value from the collection `cv-corveg:geology-types` |
| surface_strew_size |  | _what is this?_ |
| surface_strew_lithology |  | _what is this?_ |
