# Site location visit

![site_location_visit schema](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/diagrams/site_location_visit.1degree.png)


## Provisional Mapping from site_location_visit to TERN-plot
A row from an AusPlots `site_location_visit` table is mapped to an individual `plot:SiteVisit, ssn-ext:ObservationCollection` and several individual `ssn-ext:ObservationCollection`

| site_location_visit | TERN-plot | comments |
| --- | --- | --- |
| site_location_visit_id | localname (last element of the URI) | the AusPlots ID may not need to be preserved |
| site_location_id | `sosa:hasFeatureOfInterest` |  |
| visit_start_date | `prov:startedAtTime` , `sosa:phenomenonTime` |  |
| visit_end_date | `prov:endedAtTime` |  |
| photopoints_exists | n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ Can be determined by testing for the existence of an Observation with sosa:observedProperty op:XXX whose sosa:hasFeatureOfInterest/sosa:isSampleOf this site, or an Observation whose parent ObservationCollection has sosa:hasFeatureOfInterest/sosa:isSampleOf this site` |
| leaf_area_index_exists | n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ Can be determined by testing for the existence of an Observation with sosa:observedProperty op:XXX whose sosa:hasFeatureOfInterest/sosa:isSampleOf this site, or an Observation whose parent ObservationCollection has sosa:hasFeatureOfInterest/sosa:isSampleOf this site` |
| basal_wedge_has_been_collected | n/a | Can be determined by testing for the existence of an Observation with sosa:observedProperty op:XXX whose sosa:hasFeatureOfInterest/sosa:isSampleOf this site, or an Observation whose parent ObservationCollection has sosa:hasFeatureOfInterest/sosa:isSampleOf this site` |
| visit_notes | `rdfs:comment` |  |
| location_description | `dct:description` |  |
| erosion_type	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:erosion-type` <br/> and use a value from the collection _TBC_ |
| erosion_abundance	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:erosion-severity` <br/> and use a value from the collection _TBC_ <br/>_is this related to corveg erosion-severity?_ |
| erosion_state	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| microrelief	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| drainage_type	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| disturbance	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| climatic_condition	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| vegetation_condition	| `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:XXX` <br/> and use a value from the collection _TBC_ <br/>_need more information to understand what this is, whether it relates to any of the op's already assembled_ |
| observer_veg	| `ssn-ext:hasMember/prov:wasAttributedTo` | where `sosa:observedProperty op:XXX` <br/> (same as for vegetation_condition) |
| observer_soil	| `ssn-ext:hasMember/prov:wasAttributedTo` | on the associated `ssn-ext:ObservationCollection` where `dct:type ogroup:Soil` |
| described_by	| `rdfs:comment` | TBC |
| a_s_c	| `rdfs:comment` | TBC |
| pit_marker_easting	| &rarr; `plot:hadSubActivity/sosa:hasResult/w3geo:long` | Convert to WGS84 longitude. Property of a sub-site created as a result of a `sosa:Sampling` activity during this site visit ? |
| pit_marker_northing	| &rarr; `plot:hadSubActivity/sosa:hasResult/w3geo:lat` | Convert to WGS84 latitude. Property of a sub-site created as a result of a `sosa:Sampling` activity during this site visit ? |
| pit_marker_mga_zones	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ Used to convert easting/northing to long/lat |
| pit_marker_datum	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| pit_marker_location_method	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ This could be recorded as the `sosa:usedProcedure` if the location of the pit was the result of a `sosa:Observation` whose feature-of-interest is the pit. We need to consider if this information is important in the harmonized data to decide if this level of complexity is required. |
| soil_observation_type	| `rdfs:comment` | ??? |
| ok_to_publish	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| sample_location_comment	| `plot:hadSubActivity/sosa:hasResult/rdfs:comment` | TBC |
| site_visit_photo_name	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| distance_to_nearest_community	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| distance_to_nearest_infrastructure	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| comment_on_distance_to_community	| n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
| comment_on_distance_to_infrastructure | n/a | _information available at ingest but not recorded explicitly in the harmonized dataset_ |
