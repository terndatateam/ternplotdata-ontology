# Site location point

![site_location_point schema](http://ausplots-schema-schemaspy6.s3-website-ap-southeast-2.amazonaws.com/diagrams/site_location_point.1degree.png)


## Provisional Mapping from site_location_point to TERN-plot

A row from an AusPlots `site_location_point` table is mostly mapped to an individual `plot:Location`.

| site_location_point | TERN-plot | comments |
| --- | --- | --- |
| id | localname (last element of the URI) | the AusPlots ID may not need to be preserved |
| site_location_id | `plot:isLocationOf` | inverse of `locn:location` |
| point | `dct:description` |  |
| easting | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| northing | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| latitude | `locn:geometry/geo:lat` | WGS84 |
| longitude | `locn:geometry/geo:long` | WGS84 |
| zone | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| threedcq | `locn:geometry/geo:alt` | AHD _(assuming this property is actually elevation?)_ |
