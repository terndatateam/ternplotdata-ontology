# Look-up Tables

The CORVEG look-up tables were extracted as RDF, encoded using SKOS. This was straight-forward since most of the look-up tables had the same schema as seen below.

The only exception was the [Taxa](../../corveg-taxa.md) look-up table.

## Input

E.g. CORVEG table `sitetaxidentification`

| id  | orderby | code | name               | description                                                       |
|-----|---------|------|--------------------|-------------------------------------------------------------------|
| 1   | 1       | C    | Collected Specimen | Specimen has been collected at the site.                          |
| 2   | 2       | V    | Vouchered Specimen | Specimen has been vouchered (not generally recorded at the site). |
| 999 | 0       | NULL | Unrecorded         | Unrecorded.                                                       |
| 3   | 3       | F    | Field Identified   | Field Identified.                                                 |


## RDF example

```
cv-corveg:taxa-identification a owl:Ontology ;
    owl:imports <http://www.w3.org/2004/02/skos/core>,
        sosa: .

cv-corveg:taxa-identifications a skos:Collection ;
    skos:member cv-corveg:taxa-identification-C,
        cv-corveg:taxa-identification-F,
        cv-corveg:taxa-identification-Unrecorded,
        cv-corveg:taxa-identification-V ;
    skos:prefLabel "Taxa identification concepts" .

cv-corveg:taxa-identification-C a skos:Concept,
        sosa:result ;
    skos:definition "Specimen has been collected at the site." ;
    skos:hiddenLabel "1" ;
    skos:notation "C" ;
    skos:prefLabel "Collected Specimen" .

cv-corveg:taxa-identification-F a skos:Concept,
        sosa:result ;
    skos:definition "Field Identified." ;
    skos:hiddenLabel "3" ;
    skos:notation "F" ;
    skos:prefLabel "Field Identified" .

cv-corveg:taxa-identification-Unrecorded a skos:Concept,
        sosa:result ;
    skos:definition "Unrecorded." ;
    skos:hiddenLabel "999" ;
    skos:notation "null" ;
    skos:prefLabel "Unrecorded" .

cv-corveg:taxa-identification-V a skos:Concept,
        sosa:result ;
    skos:definition "Specimen has been vouchered (not generally recorded at the site)." ;
    skos:hiddenLabel "2" ;
    skos:notation "V" ;
    skos:prefLabel "Vouchered Specimen" .
```

## Mapping from CORVEG table to TERN-Plot/SKOS

| CORVEG Table | TERN-Plot/SKOS   | Comments |
|--------------|------------------|----------|
| id           | skos:hiddenLabel |          |
| orderby      | not captured     |          |
| code         | skos:notation    |          |
| name         | skos:prefLabel   |          |
| description  | skos:definition  |          |