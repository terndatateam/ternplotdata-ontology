# Rainforest structure
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's rain-forest structure (e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | COMPLEXITY_ID | LEAFSIZE_ID | FLORISTIC_ID | INDICATOR_GROWTH_FORM_ID | LEAF_CHAR_ID | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- |
| 2714 | 1 | 7 | 2 | 5 | 2 | 2289 | 2014-01-28 13:35:40 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:RFStructure` like this ([Turtle format](https://www.w3.org/TR/turtle/)):

```
corveg-rfstruct:rfs-2714 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 2714 rainforest structure observation collection" ;
    dct:modified "2014-01-28T13:35:40"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/3> ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasMember corveg-rfstruct:rfs-2714-1,
        corveg-rfstruct:rfs-2714-2,
        corveg-rfstruct:rfs-2714-3,
        corveg-rfstruct:rfs-2714-4,
        corveg-rfstruct:rfs-2714-5 .

corveg-rfstruct:rfs-2714-1 a sosa:Observation ;
    rdfs:label "Site 2714 rainforest structure structural complexity observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/rainforest-structure-complexity/s> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/42> ;
    sosa:phenomenonTime corveg-site:t1992-06-16-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-rfstruct:rfs-2714-2 a sosa:Observation ;
    rdfs:label "Site 2714 rainforest structure leaf size observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/rainforest-leafsize/7> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/46> ;
    sosa:phenomenonTime corveg-site:t1992-06-16-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-rfstruct:rfs-2714-3 a sosa:Observation ;
    rdfs:label "Site 2714 rainforest structure floristics observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/rainforest-floristics-structure/s> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/43> ;
    sosa:phenomenonTime corveg-site:site-2714 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-rfstruct:rfs-2714-4 a sosa:Observation ;
    rdfs:label "Site 2714 rainforest structure indicator growth form observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/rainforest-igf/5> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/44> ;
    sosa:phenomenonTime corveg-site:site-2714 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-rfstruct:rfs-2714-5 a sosa:Observation ;
    rdfs:label "Site 2714 rainforest structure leaf-fall characteristic observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2714 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/rainforest-deciduousness/c> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/45> ;
    sosa:phenomenonTime corveg-site:site-2714 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site:t1992-06-16-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1992-06-16T00:00:00"^^xsd:dateTime .
```

## Provisional Mapping from CORVEG.RFSTRUCTURE to TERN-plot
A row from an `CORVEG.RFSTRUCTURE` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.RFSTRUCTURE | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` |  |
| COMPLEXITY_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [rainforest structural complexity](http://linked.data.gov.au/def/corveg-cv/op/42) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/rainforest-structure-complexity. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| LEAFSIZE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [leafsize](http://linked.data.gov.au/def/corveg-cv/op/46) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/rainforest-leafsize. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| FLORISTIC_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [rainforest floristics structure](http://linked.data.gov.au/def/corveg-cv/op/43) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/rainforest-floristics-structure. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| INDICATOR_GROWTH_FORM_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [indicator growth form](http://linked.data.gov.au/def/corveg-cv/op/44) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/rainforest-igf. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| LEAF_CHAR_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [rainforest leaf-fall characteristic](http://linked.data.gov.au/def/corveg-cv/op/45) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/rainforest-deciduousness. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified`| Last data update in the database |
