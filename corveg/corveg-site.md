# Site
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

The description of a site 

## Input
e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx):

| SITE_ID | PROJECT_ID | LOCATION_ID | SITE_NUMBER | DESCRIPTION | SITE_DATE | ENTRY_DATE | LITTER_PERCENT | ROCK_PERCENT | BARE_PERCENT | CRYPTO_PERCENT | SAMPLELEVEL_ID | SAMPLETYPE_ID | SAMPLEFLORISTICS_ID | SAMPLE_AREA | HAS_PHOTO | HAS_BASAL_BYSTRATA | HAS_COVER_BYSTRATA | HAS_STEM_BYSTRATA | HAS_BASAL_BYTAXA | HAS_COVER_BYTAXA | HAS_STEM_BYTAXA | CHECKED | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 3729 | 265 | 3614 | FLAT14 | Swampy depression still holding water.  Variable 20% shrubs in patches, lower 50-1m.  Patches 80% shrubs taller 1.5m tall.  Patches where shrubs more open/variable water depths.MU175 now in RE3.2.18 after v11 review. | 07/MAY/90 | 09/JAN/18 | 18 |  | 67 |  | 3 | 3 | 3 | 30 | 0 |  |  |  |  |  |  | 1 | 121 | 2018-01-09T14:19:26 |

## Interpretation in TERN-plot
To encode each row in RDF requires

1. an individual `plot:Site` representing the site 
2. an `ssn-ext:ObservationCollection` with `dct:type ogroup:Site-description` representing the set of observations of the basic properties of the Site, composed of member `sosa:Observation` for key columns 
3. a related [plot:Location](corveg-location.md) giving the location details 

## RDF example
In ([Turtle format](https://www.w3.org/TR/turtle/) from [corveg-0.ttl](../examples/corveg-0.ttl):

```
corveg-site:sitev-29099 a plot:SiteVisit ;
    rdfs:label "CORVEG Site Visit 29099" ;
    plot:hadSubActivity corveg-site:d-29099-1 ;
    prov:endedAtTime "1995-10-13T00:00:00"^^xsd:dateTime ;
    sosa:hasFeatureOfInterest corveg-site:site-29099 .

corveg-site:site-29099 a plot:Site ;
    rdfs:label "CORVEG Site 29099" ;
    plot-x:floristics <http://linked.data.gov.au/def/corveg-cv/site-sample-floristics/c> ;
    plot-x:sampleLevel <http://linked.data.gov.au/def/corveg-cv/site-sample-level/q> ;
    plot-x:sampleType <http://linked.data.gov.au/def/corveg-cv/site-sample-type/a> ;
    dct:identifier "29099"^^corveg-def:site-id,
        "618_455029"^^corveg-def:site-number ;
    dct:modified "2015-10-20T08:44:07"^^xsd:dateTime ;
    locn:location corveg-location:l-28966 ;
    ssn-ext:hasUltimateFeatureOfInterest <http://linked.data.gov.au/def/corveg-cv/miscellaneous/87> .

corveg-site:d-29099-1 a ssn-ext:ObservationCollection ;
    rdfs:label "CORVEG Site 29099 observation collection." ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/4> ;
    sosa:hasFeatureOfInterest corveg-site:site-29099 ;
    sosa:phenomenonTime corveg-site:t1995-10-13-h0-m0-s0 ;
    sosa:resultTime "2015-10-20T00:00:00"^^xsd:dateTime ;
    ssn-ext:hasMember corveg-site:d-29099-1-1 .

corveg-site:d-29099-1-1 a sosa:Observation ;
    rdfs:label "CORVEG Site 29099 area measurement observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-29099 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:M2 ;
            rdf:value 400 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/1> ;
    sosa:phenomenonTime corveg-site:t1995-10-13-h0-m0-s0 ;
    sosa:resultTime "2015-10-20T00:00:00"^^xsd:dateTime ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/s> .

corveg-site:t1995-10-13-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1995-10-13T00:00:00"^^xsd:dateTime .
```
For convenience here the resource URIs are built from the SITE_ID. But since they are just web-keys their form is arbitrary providing they are unique and managed for the long term. 

## Mapping from CORVEG.SITE to TERN-plot
A row from an `CORVEG.SITE` table is usually mapped to an individual `plot:Site` and an associated `ssn-ext:ObservationCollection` and `plot:Location` with the columns mapped to TERN-plot properties as follows:

| CORVEG.SITE | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `dct:identifier` | the key from the CORVEG.SITE table - for traceability |
| PROJECT_ID | `prov:wasGeneratedBy` | link to description of project - this needs to be reviewed as it currently is just a dangling URI - removed until when required |
| LOCATION_ID | `locn:location` | link to description of site location |
| SITE_NUMBER | `dct:identifier` | the official externally-visible ID |
| DESCRIPTION | `dct:description` | The site description is given as an `ObservationCollection`, which carries the standard `sosa:` temporal properties |
| SITE_DATE | `plot:siteDescription/sosa:phenomenonTime` | The date when the site survey was conducted |
| ENTRY_DATE | `plot:siteDescription/sosa:resultTime` | The date when the site visit was entered in the database |
| XXXX_PERCENT | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | All quantitative measurements are members of an associated `ObservationCollection` where `sosa:observedProperty` [litter](http://linked.data.gov.au/def/corveg-cv/op/13), [rock](http://linked.data.gov.au/def/corveg-cv/op/14), [bare](http://linked.data.gov.au/def/corveg-cv/op/11), and [cryptogram](http://linked.data.gov.au/def/corveg-cv/op/12) etc and encode value using `data:Quantitative-measure`. TODO: Change observable property to a qualifier and have value as ground cover observable property and a type as either litter, cryptogram, rock, or bare. |
| SAMPLELEVEL_ID | `plot:sampleLevel` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/site-sample-level |
| SAMPLETYPE_ID | `plot:sampleType` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/site-sample-type |
| SAMPLEFLORISTICS_ID | `plot:floristics` | Scope and completeness of floristics description - may change with repeat visits and surveys. Use a value from vocabulary http://linked.data.gov.au/def/corveg-cv/site-sample-floristics |
| SAMPLE_AREA | `plot:siteDescription/ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/1 and encode value using `data:Quantitative-measure` |
| HAS_PHOTO | _not captured by TERN-plot_ |  |
| HAS_BASAL_BYSTRATA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:basal-area` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| HAS_COVER_BYSTRATA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:crown-cover` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| HAS_STEM_BYSTRATA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:stem-density` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| HAS_BASAL_BYTAXA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:basal-area` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| HAS_COVER_BYTAXA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:crown-cover` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| HAS_STEM_BYTAXA | _not encoded directly in TERN-plot_ | Can be determined by testing for the existence of an `Observation` with `sosa:observedProperty op:stem-density` whose `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site, or an `Observation` whose parent `ObservationCollection` has `sosa:hasFeatureOfInterest/sosa:isSampleOf` this site` |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` |  |

Note that (provisionally) the CORVEG classifications covering the **sample level** (primary, secondary, tertiary, quaternary) and **sample type** (rectangular, transect, hybrid, etc) are both recorded through a `dct:type` property, but using different controlled vocabularies for the values.

## Site location
A site location is described by an instance of the Location class:

- [Location](corveg-location.md)

## Observation groups
A site context is characterized through observations organized into groups recorded in separate CORVEG tables:

- [Situation](corveg-situation.md)
- [Disturbance](corveg-disturbance.md)
- [Geology](corveg-geology.md)
- [Soil](corveg-soil.md)
- [Structure](corveg-structure.md)
- [Rain forest structure](corveg-rfstructure.md)
- [Vegetation community](corveg-vegetationcommunity.md)
