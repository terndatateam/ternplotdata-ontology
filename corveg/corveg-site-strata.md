#  Site Strata
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

The summary description of a vegetation Stratum at a single sites

## Input
e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx):

| STRATA_ID | SITE_ID | DESCRIPTION | CROWNCLASS_ID | CROWNDENSITY | SPS | HTAVG | HTMIN | HTMAX | COAVG | COMIN | COMAX | RECORDED | PRESENT | UPDATE_USER | LAST_UPDATE | COVERMETHOD_ID | COVERMEASURE_ID |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 2 | 19378 |  | 999 |  | 500 | 7 | 6 | 9 | 20 |  |  | 0 | 1 | 73 | 17/AUG/15 | 3 | 1 |
| 2 | 55675 | *null* | 999 | *null* | *null* | 4 | *null* | *null* | 50 | 40 | 60 | 0 | 1 | 121 | 2015-01-30 11:20:49 | 2 | 2 |

## Interpretation in TERN-plot
To encode each row in RDF requires:
1. an individual `plot:SiteStratum` representing the sample of the site composed of a single identified stratum
2. an individual `ssn-ext:ObservationCollection` with `dct:type ogroup:Stratum` representing the set of observations of the properties of the FeatureOfInterest
3. an individual `sosa:Observation` for each column containing an observed value
4. (optionally) a collection of collections for the strata at the site

The 'proximate' feature-of-interest is a _single stratum_, which is distinct from the site-as-a-whole.
Hence, the feature-of-interest of each row is a `plot:SiteStratum` which is tagged according to its level using `plot:stratum`, and is linked to whole actual site using `sosa:isSampleOf`:

## RDF example
From [corveg-0.ttl](../examples/corveg-0.ttl)
```
corveg-site-strata:st2-55675 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 55675 stratum 2 observation collection" ;
    dct:modified "2015-01-30T11:20:49"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/9> ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/strata/t1> ;
            sosa:isSampleOf corveg-site:site-55675 ] ;
    ssn-ext:hasMember corveg-site-strata:st2-55675-1,
        corveg-site-strata:st2-55675-2,
        corveg-site-strata:st2-55675-3,
        corveg-site-strata:st2-55675-4,
        corveg-site-strata:st2-55675-5 .

corveg-site-strata:st2-55675-1 a sosa:Observation ;
    rdfs:label "Site 55675 strata 2 crown cover class observation" ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/cover-method-projection/p>,
                <http://linked.data.gov.au/def/corveg-cv/strata/t1> ] ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/miscellaneous/87> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/20> ;
    sosa:phenomenonTime corveg-site:t2000-01-01-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site-strata:st2-55675-2 a sosa:Observation ;
    rdfs:label "Site 55675 strata 2 average vegetation height observation" ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/cover-method-projection/p>,
                <http://linked.data.gov.au/def/corveg-cv/strata/t1> ] ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:M ;
            rdf:value 4.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/34> ;
    sosa:phenomenonTime corveg-site:t2000-01-01-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/34> .

corveg-site-strata:st2-55675-3 a sosa:Observation ;
    rdfs:label "Site 55675 strata 2 average crown cover observation" ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/cover-method-projection/p>,
                <http://linked.data.gov.au/def/corveg-cv/strata/t1> ] ;
    sosa:hasResult [ a data:Percent ;
            rdf:value 50.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/21> ;
    sosa:phenomenonTime corveg-site:t2000-01-01-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/16>,
        <http://linked.data.gov.au/def/corveg-cv/cover-method/v> .

corveg-site-strata:st2-55675-4 a sosa:Observation ;
    rdfs:label "Site 55675 strata 2 maximum and minimum crown cover observation" ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/cover-method-projection/p>,
                <http://linked.data.gov.au/def/corveg-cv/strata/t1> ] ;
    sosa:hasResult [ a data:PercentRange ;
            data:high 60.00 ;
            data:low 40.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/21> ;
    sosa:phenomenonTime corveg-site:t2000-01-01-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/91>,
        <http://linked.data.gov.au/def/corveg-cv/cover-method/v> .

corveg-site-strata:st2-55675-5 a sosa:Observation ;
    rdfs:label "Site 55675 strata 2 vegetation stratum observation" ;
    sosa:hasFeatureOfInterest [ a plot:SiteStratum ;
            dct:type <http://linked.data.gov.au/def/corveg-cv/cover-method-projection/p>,
                <http://linked.data.gov.au/def/corveg-cv/strata/t1> ] ;
    sosa:hasResult [ a data:Boolean ;
            rdf:value true ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/55> ;
    sosa:phenomenonTime corveg-site:t2000-01-01-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site:t2000-01-01-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "2000-01-01T00:00:00"^^xsd:dateTime .


```

## Provisional Mapping from CORVEG.SITE_STRATA to TERN-plot
A row from an `CORVEG.SITE_STRATA` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.SITE_STRATA | TERN-plot | comments |
| --- | --- | --- |
| STRATA_ID | `sosa:hasFeatureOfInterest/dct:type` | where `dct:type` and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/strata |
| SITE_ID | `sosa:hasFeatureOfInterest/sosa:isSampleOf` |  |
| DESCRIPTION | `dct:description` |  |
| CROWNCLASS_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/20 and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/crown-class. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| CROWNDENSITY | `ssn-ext:hasMember/sosa:hasResult` | No values - all are NULL. No mapping.
| SPS | _not captured by TERN-plot_ |  |
| HTAVG | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/34 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/34. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/34 |
| HTMIN | `ssn-ext:hasMember/sosa:hasResult/data:low` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/34 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/89 Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/89 |
| HTMAX | `ssn-ext:hasMember/sosa:hasResult/data:high` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/34 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/89 Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/89 |
| COAVG | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/21 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/16 and the procedure from COVERMETHOD_ID. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/16 |
| COMIN | `ssn-ext:hasMember/sosa:hasResult/data:low` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/21 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/91 and the procedure from COVERMETHOD_ID. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/91 |
| COMAX | `ssn-ext:hasMember/sosa:hasResult/data:high` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/21 with procedure http://linked.data.gov.au/def/corveg-cv/common-method/91 and the procedure from COVERMETHOD_ID. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/91 |
| RECORDED | _not captured by TERN-plot_ |  |
| PRESENT | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/55 and encode value using `data:Boolean`. Procedure used: http://linked.data.gov.au/def/corveg-cv/common-method/v. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified`| database entry time |
| COVERMETHOD_ID | `sosa:usedProcedure` | The cover method procedure for COAVG, COMAX, and COMIN in vocabulary http://linked.data.gov.au/def/corveg-cv/cover-method |
| COVERMEASURE_ID | `dct:type` | Add as `sosa:usedProcedure` for cover observations (COMAX, COMIN, COAVG) if it is a projective foliage cover or crown cover. Don't add if it is `999`, as in `unrecorded`. |
