# Situation
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's situation (e.g. these rows from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | LAND_SITUATION_ID | LAND_ELEMENT_ID | EROSION_PATTERN_ID | SLOPE_TYPE_ID | SLOPE_ANGLE | SLOPE_ERROR | ASPECT_ANGLE | ASPECT_ERROR | ALTITUDE | ALTITUDE_ERROR | UPDATE_USER | LAST_UPDATE | PATTERN_ID |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 2479 | 1 | 46 | 21 | 5 | null | null | null | null | 310 | null | 2289 | 2013-08-20 11:02:49 | 34 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:Situation` like this ([Turtle format](https://www.w3.org/TR/turtle/)):
```
corveg-situation:sit-2479 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 2479 situation observation collection" ;
    dct:modified "2013-08-20T11:02:49"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/5> ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    ssn-ext:hasMember corveg-situation:sit-2479-1,
        corveg-situation:sit-2479-2,
        corveg-situation:sit-2479-3,
        corveg-situation:sit-2479-4,
        corveg-situation:sit-2479-5,
        corveg-situation:sit-2479-6 .

corveg-situation:sit-2479-1 a sosa:Observation ;
    rdfs:label "Site 2479 land situation observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/landform-element/alc> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/38> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-situation:sit-2479-2 a sosa:Observation ;
    rdfs:label "Site 2479 land element observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/landform-element/pla> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/36> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-situation:sit-2479-3 a sosa:Observation ;
    rdfs:label "Site 2479 erosion pattern observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/erosion-pattern/up> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/28> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-situation:sit-2479-4 a sosa:Observation ;
    rdfs:label "Site 2479 slope type observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/slope-type/s> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/49> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-situation:sit-2479-5 a sosa:Observation ;
    rdfs:label "Site 2479 altitude observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard <http://www.opengis.net/def/crs/epsg/0/5711> ;
            rdf:value 310 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/0> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/landform-description/h> .

corveg-situation:sit-2479-6 a sosa:Observation ;
    rdfs:label "Site 2479 landform pattern observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-2479 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/landform-pattern/san> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/37> ;
    sosa:phenomenonTime corveg-site:t1991-05-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site:t1991-05-11-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1991-05-11T00:00:00"^^xsd:dateTime .
```

## Provisional Mapping from CORVEG.SITUATION to TERN-plot
A row from an `CORVEG.SITUATION` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.SITUATION | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` | points to a `plot:Site` |
| LAND_SITUATION_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [landform situation](http://linked.data.gov.au/def/corveg-cv/op/38) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/landform-type. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| LAND_ELEMENT_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [landform element](http://linked.data.gov.au/def/corveg-cv/op/36) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/landform-element. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| EROSION_PATTERN_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [erosional landform pattern](http://linked.data.gov.au/def/corveg-cv/op/28) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/erosion-pattern. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v' |
| SLOPE_TYPE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [slope type](http://linked.data.gov.au/def/corveg-cv/op/49) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/slope-type. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| SLOPE_ANGLE | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [slope angle](http://linked.data.gov.au/def/corveg-cv/op/48) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/a |
| SLOPE_ERROR | `ssn-ext:hasMember/sosa:hasResult/data:uncertainty` | where `sosa:observedProperty` [slope angle](http://linked.data.gov.au/def/corveg-cv/op/48) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/a |
| ASPECT_ANGLE | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [aspect angle](http://linked.data.gov.au/def/corveg-cv/op/2) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/a |
| ASPECT_ERROR | `ssn-ext:hasMember/sosa:hasResult/data:uncertainty` | where `sosa:observedProperty` [aspect angle](http://linked.data.gov.au/def/corveg-cv/op/2) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/a |
| ALTITUDE | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [altitude](http://linked.data.gov.au/def/corveg-cv/op/0) and encode value using `data:QuantitativeMeasure` where the `data:standard` is a vertical reference system from EPSG. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/h |
| ALTITUDE_ERROR | `ssn-ext:hasMember/sosa:hasResult/data:uncertainty` | where `sosa:observedProperty` [altitude](http://linked.data.gov.au/def/corveg-cv/op/0) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/landform-description/h |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` or `sosa:phenomenonTime`| database entry time |
| PATTERN_ID  | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [landform pattern](http://linked.data.gov.au/def/corveg-cv/op/37) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/landform-pattern. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
