# Alignment of CORVEG to TERN-plot
This alignment relates the (Queensland) CORVEG logical model to the O&M conceptual model.

![CORVEG logical view](./CORVEG-logical.PNG)

![CORVEG implementation view](./CORVEG-datamodel.PNG)

CORVEG reports a large set of observations of different properties of a site. The total number of potentially observed properties is large. The description of the site itself is grouped into thematic groups named `Geology`, `Disturbance`, `Situation` etc. The description of the vegetation is organized by strata. The description of the taxa at a site is generally organized by taxon, and also by strata.

# Principles of alignment of CORVEG to O&M
Following the standard observation model:

- every 'value' is recorded as the `hasResult` of an [Observation](https://www.w3.org/TR/vocab-ssn/#SOSAObservation)
- every `Observation` is related to a [FeatureOfInterest](../schema/)
    - in the context of TERN-plot this is usually a [Site](../schema/Site.md), or a [SiteStratum](../schema/SiteStratum.md), or a [SiteTaxon](../schema/SiteTaxon.md), or an [OrganismOccurrence](../schema/OrganismOccurrence.md)
- the _type_ of value is recorded as the Observation's `observedProperty`. It is taken from a controlled-vocabulary of [observable properties](https://www.w3.org/TR/vocab-ssn/#SOSAObservation)
- Observations may be _grouped_ in an [ObservationCollection](https://www.w3.org/TR/vocab-ssn-ext/#ssn-ext:ObservationCollection) which is a set of observations with (usually) a common `hasFeatureOfInterest` and `phenomenonTime`
- each individual `Observation` is nested as a `ssn-ext:hasMember` of the `ObservationCollection`

The templates on the [TERN-plot ontology description](../schema/) illustrate the application pattern.

# Advantage of using a generic model
Since the observation _type_ is indicated by properties of the observation (i.e. the `observedProperty` and `hasFeatureOfInterest`), the generic classes `Observation` and `ObservationCollection` can be used for _all_ observed data. The controlled vocabulary of `ObservableProperties` is maintained separately, and can be updated frequently, without having to touch the main data structures.

We could have defined a sub-class of `ObservationCollection` for each CORVEG table (i.e. thematic group), but this would tie the ontology directly to CORVEG, which is undesirable because

1. the CORVEG conceptual model does not match all the data submitted by the various providers, while TERN-plot - which is a lightly extended SSN/SOSA model - is flexible enough to cope
2. too much specialization/customization compromises compatibility with observation data from other sources or providers, or from other discipline areas

Where an `ObservationCollection` does correspond with a row from a CORVEG table, the name of the table can be indicated using the `dct:type` property. This may assist discovery and processing by users or applications who are familiar with CORVEG. A [list of observation groups](../cv/observation-group.ttl) is maintained as a TERN-plot 'controlled vocabulary'.


# Detailed implementation of CORVEG entities

- [Site and site description](corveg-site.md)
- [Location](corveg-location.md)
- [Situation](corveg-situation.md)
- [Disturbance](corveg-disturbance.md)
- [Geology](corveg-geology.md)
- [Soil](corveg-soil.md)
- [Structure](corveg-structure.md)
- [Rain forest structure](corveg-rfstructure.md)
- [Vegetation community](corveg-vegetationcommunity.md)
- [Site Tax Strata](corveg-site-tax-strata.md)
- [Site Strata](corveg-site-strata.md)
- [Site Taxa](corveg-site-tax.md)
- [Project](corveg-project.md)
- [Look-up Tables](cv/lookup-tables/README.md) (controlled vocabularies)
- [Taxa](corveg-taxa.md) (controlled vocabulary)

# Controlled vocabularies
Lists of values used as observation results or classifiers, are formalized in SKOS++.
These are maintained in a series of discrete collections:

- [controlled vocabularies used in data supplied by CORVEG](../cv/corveg)
- [controlled vocabularies shared across TERN](../cv/)
    - [observable properties](../cv/observable-property.ttl)
    - [observation groups](../cv/observation-group.ttl)
    - [bioregions](../cv/bioregion.ttl)

# Datatypes
Observation **results** are encoded using a set of [standard data-Datatypes](../schema/result-types.md)

# Examples
- sketch [CORVEG example](../examples/corveg-0.ttl)
