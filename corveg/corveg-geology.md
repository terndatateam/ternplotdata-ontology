# Geology
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's geology (e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | SOURCE_ID | RELIABILITY_ID | TYPECODE_ID | UNIT | ADDITIONAL | NOTES | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 1904 | 1 | 1 | 2 | Qpa | 0 | Quaternary Pleistocene deposits of clay, silt, sand and gravel, floodplain deposits on higher terraces. | 2289 | 2014-05-30T13:23:12 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:Geology` like this ([Turtle format](https://www.w3.org/TR/turtle/)):

```
corveg-geol:g-1904 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 1904 geology observation collection" ;
    plot:hasResultQuality <http://linked.data.gov.au/def/corveg-cv/geology-reliability/h> ;
    dct:modified "2014-05-30T13:23:12"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/2> ;
    rdfs:comment "Quaternary Pleistocene deposits of clay, silt, sand and gravel, floodplain deposits on higher terraces." ;
    sosa:hasFeatureOfInterest corveg-site:site-1904 ;
    ssn-ext:hasMember corveg-geol:g-1904-1,
        corveg-geol:g-1904-2 .

corveg-geol:g-1904-1 a sosa:Observation ;
    rdfs:label "Site 1904 geology lithology observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-1904 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/geology-type/b> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/31> ;
    sosa:phenomenonTime corveg-site:t1998-03-26-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/geology-source/i> .

corveg-geol:g-1904-2 a sosa:Observation ;
    rdfs:label "Site 1904 geology stratigraphic unit observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-1904 ;
    sosa:hasResult [ a data:Text ;
            rdf:value "Qpa" ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/32> ;
    sosa:phenomenonTime corveg-site:t1998-03-26-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/geology-source/i> .

corveg-site:t1998-03-26-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1998-03-26T00:00:00"^^xsd:dateTime .

```

## Provisional Mapping from CORVEG.GEOLOGY to TERN-plot
A row from an `CORVEG.GEOLOGY` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.GEOLOGY | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` | points to a `plot:Site` |
| SOURCE_ID | `sosa:usedProcedure` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/geology-source  |
| RELIABILITY_ID | `plot:hasResultQuality` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/geology-reliability  |
| TYPECODE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/31 <br/> and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/geology-type. Procedure: SOURCE_ID |
| UNIT | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/32 <br/> and encode value using `data:Text`. Procedure: SOURCE_ID |
| ADDITIONAL | _not captured by TERN-plot_ | Need to ask DES what the boolean field `ADDITIONAL` means so that we can capture it, if it's useful.  |
| NOTES | `rdfs:comment` |  |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` or `sosa:phenomenonTime`| the database entry time |
