# Structure
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's structure (e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | SF_CLASSIFICATION_ID | STRUCTURAL_FORM_ID | BASAL_AREA_PER_HA | CROWN_COVER_PER_HA | STEM_DENSITY_PER_HA | UPDATE_USER | LAST_UPDATE | BA_FACTOR_ID |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 3758 | 1 | 8 | 6 |  | 330 | 5 | 2007-05-18 13:08:45 | 3 |
| 13864 | 1 | 6 | 18 | 65 | 300 | 55 | 2007-10-23 05:13:30 | 3 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:Structure` like this ([Turtle format](https://www.w3.org/TR/turtle/)):

```
corveg-struct:struct-13864 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 13864 structure observation collection" ;
    dct:modified "2007-10-23T05:13:30"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/7> ;
    sosa:hasFeatureOfInterest corveg-site:site-13864 ;
    ssn-ext:hasMember corveg-struct:struct-13864-1,
        corveg-struct:struct-13864-2,
        corveg-struct:struct-13864-3,
        corveg-struct:struct-13864-4 .

corveg-struct:struct-13864-1 a sosa:Observation ;
    rdfs:label "Site 13864 structural formation observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-13864 ;
    sosa:hasResult [ a data:Concept ;
            data:standard <http://linked.data.gov.au/def/corveg-cv/structural-formation-classification-system/s> ;
            rdf:value <http://linked.data.gov.au/def/corveg-cv/structure-formation/of> ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/56> ;
    sosa:phenomenonTime corveg-site:t1996-06-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-struct:struct-13864-2 a sosa:Observation ;
    rdfs:label "Site 13864 basal area observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-13864 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:PER-HA ;
            rdf:value 18.00 ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/4> ;
    sosa:phenomenonTime corveg-site:t1996-06-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/structure-basal-factor/1> .

corveg-struct:struct-13864-3 a sosa:Observation ;
    rdfs:label "Site 13864 crown cover per hectare observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-13864 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:PER-HA ;
            rdf:value 65 ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/21> ;
    sosa:phenomenonTime corveg-site:t1996-06-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/c> .

corveg-struct:struct-13864-4 a sosa:Observation ;
    rdfs:label "Site 13864 stem density per hectare observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-13864 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:PER-HA ;
            rdf:value 300 ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/54> ;
    sosa:phenomenonTime corveg-site:t1996-06-11-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/c> .

corveg-site:t1996-06-11-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1996-06-11T00:00:00"^^xsd:dateTime .

```

## Provisional Mapping from CORVEG.STRUCTURE to TERN-plot
A row from an `CORVEG.STRUCTURE` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.STRUCTURE | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` |  |
| SF_CLASSIFICATION_ID | `ssn-ext:hasMember/sosa:hasResult/data:standard` | where `sosa:observedProperty` [structural formation](http://linked.data.gov.au/def/corveg-cv/op/56) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/structural-formation-classification-system, data:standard is considered because we believe the outcome is a standard code to represent vegetation structure. |
| STRUCTURAL_FORM_ID | `ssn-ext:hasMember/sosa:hasResult/rdf:value` | where `sosa:observedProperty` [structural formation](http://linked.data.gov.au/def/corveg-cv/op/56) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/structure-formation. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| BASAL_AREA_PER_HA | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [basal area](http://linked.data.gov.au/def/corveg-cv/op/4) and encode value using `data:QuantitativeMeasure`. Procedure: BA_FACTOR_ID |
| CROWN_COVER_PER_HA | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [crown cover](http://linked.data.gov.au/def/corveg-cv/op/21) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/c |
| STEM_DENSITY_PER_HA | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [stem density](http://linked.data.gov.au/def/corveg-cv/op/54) and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/c |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` | database entry time |
| BA_FACTOR_ID | `sosa:usedProcedure ` | Use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/structure-basal-factor. Note: This is a value in the [Wedge Prizm](https://en.wikipedia.org/wiki/Wedge_prism) instrument used to measure basal area per hectare (i.e. in this case the value of BASAL_AREA_PER_HA) |
