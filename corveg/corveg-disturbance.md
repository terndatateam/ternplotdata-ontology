# Disturbance
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's disturbance (e.g. these rows from the [extract from CORVEG](corveg_top1000.xlsx)):

| DISTURBANCE_ID | SITE_ID | TYPE_ID | PROPORTION_ID | AGE_ID | HEIGHT_ID | GRAZINGLEVEL_ID | EROSION_ID | EROSIONSEVERITY_ID | COUNT | UPDATE_USER | LAST_UPDATE | WEED_COVER |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 11595 | 23362 | 3 | 4 | 2 | 2 | 999 | 999 | 999 |  | 118 | 22/JUL/10 |   |

may be encoded in RDF with each row as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:Disturbance` like this ([Turtle format](https://www.w3.org/TR/turtle/)):

```
corveg-dist:d- a ssn-ext:ObservationCollection ;
    rdfs:label "Site 23362 disturbance 11595 observation collection" ;
    dct:modified "2010-07-22T11:30:59"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/disturbance-type/fire>,
        <http://linked.data.gov.au/def/corveg-cv/ogroup/1> ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:phenomenonTime corveg-dist:t1999-08-18-h0-m0-s0 ;
    ssn-ext:hasMember corveg-dist:d-11595-1,
        corveg-dist:d-11595-2,
        corveg-dist:d-11595-3,
        corveg-dist:d-11595-4,
        corveg-dist:d-11595-5,
        corveg-dist:d-11595-6 .

corveg-dist:d-11595-1 a sosa:Observation ;
    rdfs:label "Disturbance 11595 proportion observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/disturbance-proportion/3> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/26> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-dist:d-11595-2 a sosa:Observation ;
    rdfs:label "Disturbance 11595 age observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/disturbance-age/2> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/24> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-dist:d-11595-3 a sosa:Observation ;
    rdfs:label "Disturbance 11595 height observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/disturbance-height/2> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/25> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-dist:d-11595-4 a sosa:Observation ;
    rdfs:label "Disturbance 11595 grazing level observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/miscellaneous/87> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/33> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-dist:d-11595-5 a sosa:Observation ;
    rdfs:label "Disturbance 11595 erosion observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/miscellaneous/87> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/30> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-dist:d-11595-6 a sosa:Observation ;
    rdfs:label "Disturbance 11595 erosion severity observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-23362 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/miscellaneous/87> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/29> ;
    sosa:phenomenonTime corveg-site:t1999-08-18-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1> .

corveg-site:t1999-08-18-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1999-08-18T00:00:00"^^xsd:dateTime .
```

## Provisional Mapping from CORVEG.DISTURBANCE to TERN-plot
A row from an `CORVEG.DISTURBANCE` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.DISTURBANCE | TERN-plot | comments |
| --- | --- | --- |
| DISTURBANCE_ID | localname (last element of the URI) | the CORVEG ID may not need to be preserved, but we use it to create the URI identifier for the ObservationCollections and Observations. |
| SITE_ID | `sosa:hasFeatureOfInterest` | points to a `plot:Site` in the ObservationCollection. |
| TYPE_ID | `dct:type` | with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/disturbance-type |
| PROPORTION_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/26 with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/disturbance-proportion. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1 |
| AGE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/24 <br/> with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/disturbance-age. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1  |
| HEIGHT_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/25 with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/disturbance-height. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1  |
| GRAZINGLEVEL_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/33 with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/grazing-severity. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1  |
| EROSION_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/30 with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/erosion-type. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1  |
| EROSIONSEVERITY_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/29 with a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/erosion-severity. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1  |
| COUNT |`ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/40 if TYPE_ID value is 5 or `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/41 if TYPE_ID value is 6 and encode value as `data:Count`. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1 |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` or `sosa:phenomenonTime`| last database update time |
| WEED_COVER | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/15 and value encoded as `data:Percent`. Procedure: http://linked.data.gov.au/def/corveg-cv/disturbance-assessment/1 |
