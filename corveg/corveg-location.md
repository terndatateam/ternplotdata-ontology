# Location
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a location (e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx)):

| LOCATION_ID | DERIVATION_ID | MAP_NAME | MAP_NUMBER | MAP_SCALE_ID | PASTORAL_DISTRICT | LOCALITY | LOCATION_PRECISION | LOCATION_ZONE | EASTING | NORTHING | LATITUDE | LONGITUDE | UPDATE_USER | LAST_UPDATE | BIOREGION_ID | IS_DD_ORIGINAL |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 3614 | 2 | CAPE MELVILLE | 7968 | 2 | COOK | APPROXIMATELY 8 KM NW OF CAPE FLATTERY TOWNSHIP, ON THE ROAD TO LOOKOUT POINT |  | 55 | 311895 | 8348476 | -14.97486704 | 145.2510978 | 5 | 2007-05-18T13:08:00 | 4 | 0 |

may be encoded in RDF as an instance of `plot:Location` like this ([Turtle format](https://www.w3.org/TR/turtle/)):

```
corveg-location:l-3614 a plot:Location ;
    rdfs:label "Location 3614" ;
    plot:mapScale <http://linked.data.gov.au/def/corveg-cv/mapscale/100000> ;
    plot:mapsheetName "CAPE MELVILLE" ;
    plot:mapsheetNumber "7968" ;
    dct:description "APPROXIMATELY 8 KM NW OF CAPE FLATTERY TOWNSHIP, ON THE ROAD TO LOOKOUT POINT" ;
    dct:identifier 3614 ;
    dct:modified "2007-05-18T13:08:00"^^xsd:dateTime ;
    locn:geographicName "COOK" ;
    locn:geometry [ a geo:Point ;
            geo:lat -14.93181180 ;
            geo:long 145.25109782 ] ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/location-derivation/b> ;
    ssn-ext:hasUltimateFeatureOfInterest <http://linked.data.gov.au/def/corveg-cv/bioregion-qld/cyp> .


```

For convenience here the resource URI is built from the LOCATION_ID. But since it is just a web-key its form is arbitrary providing it is unique and managed for the long term.


## Provisional Mapping from CORVEG.LOCATION to TERN-plot
A row from an `CORVEG.LOCATION` table is mapped to an individual `plot:Location` with the columns mapped to TERN-plot properties as follows:

| CORVEG.LOCATION | TERN-plot | comments |
| --- | --- | --- |
| LOCATION_ID | localname (last element of the URI) | the CORVEG ID may not need to be preserved |
| DERIVATION_ID | `sosa:usedProcedure` | a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/location-derivation |
| MAP_NAME | `plot:mapsheetName` |  |
| MAP_NUMBER | `plot:mapsheetNumber` |  |
| MAP_SCALE | `plot:mapScale` | a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/mapscale |
| PASTORAL_DISTRICT | `locn:geographicName` |  |
| LOCALITY | `dct:description` |  |
| LOCATION_PRECISION | _not captured by TERN-plot_ |  |
| LOCATION_ZONE | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| EASTING | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| NORTHING | _not captured explicitly by TERN-plot_ | Can be derived from lat/long data |
| LATITUDE | `locn:geometry/geo:lat` | WGS84 |
| LONGITUDE | `locn:geometry/geo:long` | WGS84 |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` |  |
| BIOREGION |  | recorded as `ssn-ext:hasUltimateFeatureOfInterest` of the associated Site, value from vocabulary http://linked.data.gov.au/def/corveg-cv/bioregion-qld |
| IS_DD_ORIGINAL | _not captured by TERN-plot_ | a boolean value, not sure what it is. |
