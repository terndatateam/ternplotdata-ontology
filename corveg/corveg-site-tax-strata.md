#  Site-Tax-Strata
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

Description of an assemblage (i.e community composed of a single taxon) in a specified vegetation stratum at a single site.

## Input
e.g. these rows from the `CORVEG.SITE_TAX_STRATA` table in [extract from CORVEG](corveg_top1000.xlsx):

| LINNAEAN_ID | SITE_ID | STRATA_ID | BASAL_AREA | CROWN_COVER | STEM_DENSITY | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- |
| 5129 | 16469 | 3 | 4 | 1 | 7 | 83 | 2008-10-15 15:22:46 |
| 559 | 16469 | 3 | 1 | 5 | 1 | 83 | 2008-10-15 15:22:46 |
| 559 | 16469 | 5 |  | 0.1 | 4 | 83 | 2008-10-15 15:22:46 |

## Interpretation in TERN-plot
this table represents obervations on taxon at each of the stratum of a site.
To encode each row in RDF requires
1. an individual `plot:SiteStratumTaxon` representing the sample of the site composed of a single designated taxon in a single identified stratum
2. an individual `ssn-ext:ObservationCollection` with `dct:type ogroup:Site-Strata-Taxa` representing the set of observations of the properties of the FeatureOfInterest
3. an individual `sosa:Observation` for each column containing an observed value

The `plot:SiteStratumTaxon` which is the feature-of-interest of the collection and of each of its members is tagged according to its level using `plot:stratum`, according to its taxon with `plot:taxon`, and is linked to the site using `sosa:isSampleOf`.

## RDF example
From [corveg-0.ttl](../examples/corveg-0.ttl)
```
corveg-site-tax-strata:s16469-st3-t5129-oc1 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 16469 stratum 3 taxon 5129 observation collection" ;
    dct:modified "2008-10-15T15:22:46"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/10> ;
    sosa:isSampleOf corveg-site-tax-strata:sst-16469-3-5129 ;
    ssn-ext:hasMember corveg-site-tax-strata:s16469-st3-t5129-o1,
        corveg-site-tax-strata:s16469-st3-t5129-o2,
        corveg-site-tax-strata:s16469-st3-t5129-o3 .

corveg-site-tax-strata:s16469-st3-t5129-o1 a sosa:Observation ;
    rdfs:label "Site 16469 stratum 3 taxon 5129 basal area observation" ;
    sosa:hasFeatureOfInterest corveg-site-tax-strata:sst-16469-3-5129 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:M2 ;
            rdf:value 4.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/4> ;
    sosa:phenomenonTime corveg-site:t1997-04-10-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/b> .

corveg-site-tax-strata:s16469-st3-t5129-o2 a sosa:Observation ;
    rdfs:label "Site 16469 stratum 3 taxon 5129 crown cover observation" ;
    sosa:hasFeatureOfInterest corveg-site-tax-strata:sst-16469-3-5129 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:M2 ;
            rdf:value 1.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/21> ;
    sosa:phenomenonTime corveg-site:t1997-04-10-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/cover-method/v> .

corveg-site-tax-strata:s16469-st3-t5129-o3 a sosa:Observation ;
    rdfs:label "Site 16469 stratum 3 taxon 5129 stem density observation" ;
    sosa:hasFeatureOfInterest corveg-site-tax-strata:sst-16469-3-5129 ;
    sosa:hasResult [ a data:QuantitativeMeasure ;
            data:standard unit:PER-M2 ;
            rdf:value 7.00 ] ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/54> ;
    sosa:phenomenonTime corveg-site:t1997-04-10-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/c>,
        <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site:t1997-04-10-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1997-04-10T00:00:00"^^xsd:dateTime .

corveg-site-tax-strata:sst-16469-3-5129 a plot:SiteStratumTaxon ;
    rdfs:label "Site 16469 stratum 3 taxon 5129" ;
    plot:stratum <http://linked.data.gov.au/def/corveg-cv/strata/t2> ;
    plot:taxon corveg-taxa:leptospermum-trinervium ;
    sosa:isSampleOf corveg-site:site-16469 .
```

## Mapping from CORVEG.SITE_TAX_STRATA to TERN-plot
A row from an `CORVEG.SITE_TAX_STRATA` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.SITE_TAX_STRATA | TERN-plot | comments |
| --- | --- | --- |
| LINNEAN_ID | `sosa:hasFeatureOfInterest/plot:taxon` | link to the taxon from Taxa table |
| SITE_ID | `sosa:hasFeatureOfInterest/sosa:isSampleOf` | link to an individual Site |
| STRATA_ID | `sosa:hasFeatureOfInterest/plot:stratum` | a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/strata |
| BASAL_AREA | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/4 and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/b |
| CROWN_COVER | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/21 and encode value using `data:QuantitativeMeasure`. Procedure: From table Site_Strata and its column COVERMETHOD_ID, match to a term in the vocabulary http://linked.data.gov.au/def/corveg-cv/cover-method |
| STEM_DENSITY | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` http://linked.data.gov.au/def/corveg-cv/op/22 and encode value using `data:QuantitativeMeasure`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/c and http://linked.data.gov.au/def/corveg-cv/common-method/v |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dcterms:modified`|  |
