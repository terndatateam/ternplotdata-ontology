# Site Tax (deprecated, see [Site Tax Strata](corveg-site-tax-strata.md) instead)
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

Summary description of an assemblage (i.e community composed of a single taxon) at a single site.

## Input
e.g. this row from the [extract from CORVEG](corveg_top1000.xlsx):

| LINNAEAN_ID | SITE_ID | IDENTIFICATION_ID | CROWN_TYPE | BASAL_AREA | CROWN_COVER | STEM_DENSITY | NON_STANDARD_ABUNDANCE | DESCRIPTION | MISCELLANEOUS | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 12849 | 3727 | 2 |  |  |  |  |  | AQ484041 | 0 | 79 | 03/OCT/13 |
| 5370 | 3727 | 2 |  |  |  |  |  | AQ627304 | 0 | 79 | 03/OCT/13 |

## Interpretation in TERN-plot
To encode each row in RDF requires:
1. an individual `plot:SiteTaxon` representing the sample of the site composed of a single designated taxon
2. an individual `ssn-ext:ObservationCollection` with `dct:type ogroup:Taxon` representing the set of observations of the properties of the FeatureOfInterest
3. an individual `sosa:Observation` for each column containing an observed value
4. (optionally) a collection of collections for the taxa at the site

The 'proximate' feature-of-interest is a _single taxon community_, which is distinct from the site-as-a-whole.
Hence the feature-of-interest of each row is a `plot:SiteTaxon` which is tagged according to its assemblage taxon using `plot:taxon`, which is linked to the whole site using `sosa:isSampleOf`.

Strictly, the taxon identity is the result of an observation, which uses a specified identification procedure. The result is a taxon denoted by a Linnean ID or an ID from another system. This determination is one member of an observation-collection related to this FOI, alongside measures and other statistics about the community.

## RDF example
From [corveg-0.ttl](../examples/corveg-0.ttl)
```
ex-0:ST-3727-12849
  rdf:type plot:SiteTaxon ;
  plot:taxon <http://gbif.example.org/id/linnean/12849> ;
  sosa:isSampleOf ex-0:Site-3727 ;
.
ex-0:Tax-1-3727
  rdf:type ssn-ext:ObservationCollection ;
  dct:type ogroup:SiteTaxon ;
  rdfs:comment "AQ484041" ;
  rdfs:label "Site 3727 taxon 1" ;
  sosa:hasFeatureOfInterest ex-0:ST-3727-12849 ;
  sosa:phenomenonTime ex-0:T2013-10-03 ;
  ssn-ext:hasMember ex-0:Tax-1-3727-1 ;
  ssn-ext:hasMember ex-0:Tax-1-3727-2 ;
.
ex-0:Tax-1-3727-1
  rdf:type sosa:Observation ;
  sosa:hasFeatureOfInterest ex-0:ST-3727-12849 ;
  sosa:hasResult <http://gbif.example.org/id/linnean/12849> ;
  sosa:observedProperty op:taxon-identity ;
  sosa:phenomenonTime ex-0:T2013-10-03 ;
  sosa:usedProcedure cv-corveg:taxa-identification-V ;
.
ex-0:Tax-1-3727-2
  rdf:type sosa:Observation ;
  sosa:hasFeatureOfInterest ex-0:ST-3727-12849 ;
  sosa:hasResult [
      rdf:type data:Quantitative-measure ;
      rdf:value "unknown" ;
    ] ;
  sosa:observedProperty op:basal-area ;
  sosa:phenomenonTime ex-0:T2013-10-03 ;
.
ex-0:ST-3727-5370
  rdf:type plot:SiteTaxon ;
  plot:taxon <http://gbif.example.org/id/linnean/5370> ;
  sosa:isSampleOf ex-0:Site-3727 ;
.
ex-0:Tax-2-3727
  rdf:type ssn-ext:ObservationCollection ;
  dct:type ogroup:SiteTaxon ;
  rdfs:comment "AQ627304" ;
  rdfs:label "Site 3727 taxon 2" ;
  sosa:hasFeatureOfInterest ex-0:ST-3727-5370 ;
  sosa:phenomenonTime ex-0:T2013-10-03 ;
  ssn-ext:hasMember ex-0:Tax-2-3727-1 ;
.
ex-0:Tax-2-3727-1
  rdf:type sosa:Observation ;
  sosa:hasFeatureOfInterest ex-0:ST-3727-5370 ;
  sosa:hasResult <http://gbif.example.org/id/linnean/5370> ;
  sosa:observedProperty op:taxon-identity ;
  sosa:phenomenonTime ex-0:T2013-10-03 ;
  sosa:usedProcedure cv-corveg:taxa-identification-V ;
.
ex-0:Taxa-3727
  rdf:type ssn-ext:ObservationCollection ;
  dcterms:type ogroup:SiteTaxa ;
  dcterms:type cv-corveg:site-sample-floristics-B ;
  rdfs:label "The taxa observations at site 3727" ;
  sosa:hasFeatureOfInterest ex-0:Site-3727 ;
  ssn-ext:hasMember ex-0:Tax-1-3727 ;
  ssn-ext:hasMember ex-0:Tax-2-3727 ;
.
```

## Mapping from CORVEG.SITE_TAX to TERN-plot
A row from an `CORVEG.SITE_TAX` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.SITE_TAX | TERN-plot | comments |
| --- | --- | --- |
| LINNAEAN_ID | `ssn-ext:hasMember/plot:taxon` | use a value from a recognised source of taxa |
| LINNAEAN_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:taxon-identity` <br/> and use a value from a recognised source of taxa |
| SITE_ID | `sosa:hasFeatureOfInterest/sosa:isSampleOf` |  |
| IDENTIFICATION_ID | `ssn-ext:hasMember/sosa:usedProcedure` | where `sosa:observedProperty op:taxon-identity` <br/> and use a value from the collection `cv-corveg:taxa-identifications` |
| CROWN_TYPE | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:crown-class` <br/> and use a value from the collection `cv-corveg:crown-classes` |
| BASAL_AREA | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:basal-area` <br/> and encode value using `data:Quantitative-measure` |
| CROWN_COVER | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:crown-cover` <br/> and encode value using `data:Quantitative-measure` |
| STEM_DENSITY | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty op:stem-density` <br/> and encode value using `data:Quantitative-measure` |
| NON_STANDARD_ABUNDANCE | _not captured by TERN-plot_ |  |
| DESCRIPTION | `dct:description` |  |
| MISCELLANEOUS | `rdfs:comment` |  |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` or `sosa:phenomenonTime`| is this the database-entry time, or the observation time? |

Note that `LINNEAN_ID` appears in two places in the RDF representation:
1. as the value of the `plot:taxon` property of the `plot:SiteTaxon` which is part of the key for the FeatureOfInterest
2. as the result of the `sosa:Observation` whose `sosa:observedProperty` is `op:taxon-identity`
