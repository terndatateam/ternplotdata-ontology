# Vegetation community
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's vegetation community (e.g. these rows from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | VEG_UNIT | UVEG_NUMBER | RE | COMMUNITY_CONTEXT | COMMUNITYEXTENT_ID | COMMUNITYAREA_ID | REPRESENTATIVE | MAPPED | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 3729 | 175 |    |         | m.arcana, l.polygalifolium ssp. tropicum open heath with occasional shrubland | 999 | 999 | 1 | 1 | 121 | 2018-01-09T14:19:18 |
| 3819 | 2   | n2 | 3.5.36b | E.TETRODONTA, E.NESOPHILA MEDIUM TALL WOODLAND                                | 6 | 6 | 1 | 1 | 119 | 06/JUL/16 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:VegetationCommunity` like this ([Turtle format](https://www.w3.org/TR/turtle/)):
```
corveg-v-comm:vege-comm-3819 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 3819 vegetation community observation collection" ;
    dct:modified "2016-07-06T15:39:19"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/8> ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    ssn-ext:hasMember corveg-v-comm:vege-comm-3819-1,
        corveg-v-comm:vege-comm-3819-2,
        corveg-v-comm:vege-comm-3819-3,
        corveg-v-comm:vege-comm-3819-4,
        corveg-v-comm:vege-comm-3819-5 .

corveg-v-comm:vege-comm-3819-1 a sosa:Observation ;
    rdfs:label "Site 3819 vegetation community site context observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    sosa:hasResult [ a data:Text ;
            rdf:value "e.tetrodonta, e.nesophila medium tall woodland" ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/6> ;
    sosa:phenomenonTime corveg-site:t1991-04-24-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-v-comm:vege-comm-3819-2 a sosa:Observation ;
    rdfs:label "Site 3819 vegetation community extent observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/community-extent/f> ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/7> ;
    sosa:phenomenonTime corveg-site:t1991-04-24-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-v-comm:vege-comm-3819-3 a sosa:Observation ;
    rdfs:label "Site 3819 vegetation community area observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/community-area/f> ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/5> ;
    sosa:phenomenonTime corveg-site:t1991-04-24-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-v-comm:vege-comm-3819-4 a sosa:Observation ;
    rdfs:label "Site 3819 vegetation community ecosystem representation observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    sosa:hasResult [ a data:Boolean ;
            rdf:value true ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/9> ;
    sosa:phenomenonTime corveg-site:t1991-04-24-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-v-comm:vege-comm-3819-5 a sosa:Observation ;
    rdfs:label "Site 3819 vegetation community ecosystem mapping observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3819 ;
    sosa:hasResult [ a data:Boolean ;
            rdf:value true ] ;
    sosa:observableProperty <http://linked.data.gov.au/def/corveg-cv/op/8> ;
    sosa:phenomenonTime corveg-site:t1991-04-24-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/common-method/v> .

corveg-site:t1991-04-24-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1991-04-24T00:00:00"^^xsd:dateTime .
```

## Provisional Mapping from CORVEG.VEGETATIONCOMMUNITY to TERN-plot
A row from an `CORVEG.VEGETATIONCOMMUNITY` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:

| CORVEG.VEGETATIONCOMMUNITY | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` |  |
| VEG_UNIT |  | Not captured - internal use only. According to Dale Richter from DES: *This is the botanists photo pattern code or vegetation code. We don’t publish this field as it only means something to the mapper.* |
| UVEG_NUMBER | | Not captured - internal use only. According to Dale Richter from DES: *We no longer use this. This was a universal vegetation code that was unique across the state.* |  |
| RE |  | Not captured - internal use only. According to Dale Richter from DES: *This is the Regional Ecosystem code.  This is our published classification descriptor.  We don’t currently publish this through TERN to the public.* |
| COMMUNITY_CONTEXT | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [community context](http://linked.data.gov.au/def/corveg-cv/op/6) and encode value using `data:Text`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| COMMUNITYEXTENT_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [community extent](http://linked.data.gov.au/def/corveg-cv/op/7) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/community-extent. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| COMMUNITYAREA_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [community area](http://linked.data.gov.au/def/corveg-cv/op/5) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/community-area. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| REPRESENTATIVE | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [community representative](http://linked.data.gov.au/def/corveg-cv/op/9) and encode value using `data:Boolean`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| MAPPED | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [community mapped](http://linked.data.gov.au/def/corveg-cv/op/8) and encode value using `data:Boolean`. Procedure: http://linked.data.gov.au/def/corveg-cv/common-method/v |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` | database entry time |
