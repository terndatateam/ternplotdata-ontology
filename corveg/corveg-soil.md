# Soil
- [RDF Principles](../schema/rdf-principles.md)
- Details of [TERN-plot ontology](../schema/)
- Details of [RDF prefixes](../schema/prefixes.md)
- Details of [result types](../schema/result-types.md)
- [Controlled vocabularies](../cv/)

## TERN-plot example
The description of a site's soil (e.g. these rows from the [extract from CORVEG](corveg_top1000.xlsx)):

| SITE_ID | SOURCE_ID | RELIABILITY_ID | TYPECODE_ID | TS_COLOUR_ID | TS_TEXTURE_ID | TS_PH | ISBELL_ID | ADDITIONAL | NOTES | UPDATE_USER | LAST_UPDATE |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
|  3729 | 4 | 2 | 1 | 2 | 6 |  | 999 | 0 | Top soil texture: sand.  Top soil colour: grey/brown.  Stoniness: absent. | 121 | 2012-02-27T09:19:39 |

may be encoded in RDF as an instance of `ssn-ext:ObservationCollection` with `dct:type ogroup:Soil` like this ([Turtle format](https://www.w3.org/TR/turtle/)):
```
corveg-soil:soil-3729 a ssn-ext:ObservationCollection ;
    rdfs:label "Site 3729 soil observation collection" ;
    plot:hasResultQuality <http://linked.data.gov.au/def/corveg-cv/soil-reliability/m> ;
    dct:modified "2012-02-27T09:19:39"^^xsd:dateTime ;
    dct:type <http://linked.data.gov.au/def/corveg-cv/ogroup/6> ;
    rdfs:comment "Top soil texture: sand.  Top soil colour: grey/brown.  Stoniness: absent." ;
    sosa:hasFeatureOfInterest corveg-site:site-3729 ;
    sosa:phenomenonTime corveg-site:t1990-05-07-h0-m0-s0 ;
    ssn-ext:hasMember corveg-soil:soil-3729-1,
        corveg-soil:soil-3729-2,
        corveg-soil:soil-3729-3 .

corveg-soil:soil-3729-1 a sosa:Observation ;
    rdfs:label "Site 3729 soil type observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3729 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/soil-type/a> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/53> ;
    sosa:phenomenonTime corveg-site:t1990-05-07-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/soil-source/s> .

corveg-soil:soil-3729-2 a sosa:Observation ;
    rdfs:label "Site 3729 soil colour observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3729 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/soil-colour/b> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/50> ;
    sosa:phenomenonTime corveg-site:t1990-05-07-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/soil-source/s> .

corveg-soil:soil-3729-3 a sosa:Observation ;
    rdfs:label "Site 3729 soil texture observation" ;
    sosa:hasFeatureOfInterest corveg-site:site-3729 ;
    sosa:hasResult <http://linked.data.gov.au/def/corveg-cv/soil-texture/f> ;
    sosa:observedProperty <http://linked.data.gov.au/def/corveg-cv/op/52> ;
    sosa:phenomenonTime corveg-site:t1990-05-07-h0-m0-s0 ;
    sosa:usedProcedure <http://linked.data.gov.au/def/corveg-cv/soil-source/s> .

corveg-site:t1990-05-07-h0-m0-s0 a time:Instant ;
    time:inXSDDateTime "1990-05-07T00:00:00"^^xsd:dateTime .

```

## Provisional Mapping from CORVEG.SOIL to TERN-plot
A row from an `CORVEG.SOIL` table is usually mapped to an individual `ssn-ext:ObservationCollection` with the columns mapped to TERN-plot properties as follows:


| CORVEG.SOIL | TERN-plot | comments |
| --- | --- | --- |
| SITE_ID | `sosa:hasFeatureOfInterest` | points to a `plot:Site` |
| SOURCE_ID | `sosa:usedProcedure` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/soil-source |
| RELIABILITY_ID | `plot:hasResultQuality` | use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/soil-reliability |
| TYPECODE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [soil type](http://linked.data.gov.au/def/corveg-cv/op/53) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/soil-type. Procedure: SOURCE_ID |
| TS_COLOUR_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [soil colour](http://linked.data.gov.au/def/corveg-cv/op/50) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/soil-colour. Procedure: SOURCE_ID |
| TS_TEXTURE_ID | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [soil texture](http://linked.data.gov.au/def/corveg-cv/op/52) and use a value from the vocabulary http://linked.data.gov.au/def/corveg-cv/soil-texture. Procedure: SOURCE_ID  |
| TS_PH | `ssn-ext:hasMember/sosa:hasResult` | where `sosa:observedProperty` [soil pH](http://linked.data.gov.au/def/corveg-cv/op/51) and encode value using `data:QuantitativeMeasure`. Procedure: SOURCE_ID  |
| ISBELL_ID | `_not captured by CORVEG_ |  (currently the only values are *null* or 999, i.e. unrecorded). Procedure: SOURCE_ID  |
| ADDITIONAL | _not captured by TERN-plot_ | boolean values are present. |
| NOTES | `rdfs:comment` |  |
| UPDATE_USER | _not captured by TERN-plot_ |  |
| LAST_UPDATE | `dct:modified` ~~or `sosa:phenomenonTime`~~| this is the database entry time, the observation time is either the site_date or the entry_date in the CORVEG.SITE table |
